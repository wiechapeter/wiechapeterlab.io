---
title: pyGDM
subtitle: A python toolkit for electro-dynamical simulations
date: 2024-07-02
---

For recent updates, please see the pyGDM [changelog](https://homepages.laas.fr/pwiecha/pygdm_doc/changelog.html).

pyGDM is a python toolkit for simple and rapid electro-dynamical simulations of photonic nanostructures. 
If you are interested, have a look at the [<span style="color:orange">paper</span>](https://arxiv.org/abs/1802.04071), or the [paper on recent updates](http://arxiv.org/abs/2105.04587) or try some of the tutorials and examples from the [<span style="color:orange">online documentation</span>](https://homepages.laas.fr/pwiecha/pygdm_doc/index.html).
You can clone the package from [<span style="color:orange">gitlab</span>](https://gitlab.com/wiechapeter/pyGDM2) or download it from [<span style="color:orange">pypi</span>](https://pypi.python.org/pypi/pygdm2/) (install via *$ pip install pygdm2*). 

 - pyGDM paper: [Computer Physics Communications 233, 167-192 (2018)](https://doi.org/10.1016/j.cpc.2018.06.017)
 - pyGDM update paper: [Computer Physics Communications 270, 108142 (2022)](https://doi.org/10.1016/j.cpc.2021.108142)
 - online documentation: [https://homepages.laas.fr/pwiecha/pygdm_doc/index.html](https://homepages.laas.fr/pwiecha/pygdm_doc/index.html)

