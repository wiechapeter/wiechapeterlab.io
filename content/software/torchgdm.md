---
title: TorchGDM
subtitle: Auto-diff enabled electro-dynamical simulations
date: 2024-07-03
---

For recent updates, please see the torchGDM [changelog](https://gitlab.com/wiechapeter/torchgdm/-/blob/main/changelog.md).

TorchGDM is a PyTorch implementation of the [Green's dyadic method (GDM)](https://doi.org/10.1088/0034-4885/68/8/R05), a electro-dynamics full-field volume integral technique. It's main features are mixed simulations combining volume discretized and effective e/m dipole-pair polarizabilities, as well as the general support of [torch's automatic differentiation](https://pytorch.org/).
TorchGDM is originally based on various theoretical works by Christian Girard at CEMES (see e.g. [Ch. Girard 2005 Rep. Prog. Phys. 68 1883](https://doi.org/10.1088/0034-4885/68/8/R05)), with contributions from G. Colas des Francs, A. Arbouet, R. Marty, C. Majorel, A. Patoux, Y. Brûlé and P. R. Wiecha.

TorchGDM is currently in a beta version, with the main API almost finalized and having already passed an extensive test phase. Various [examples can be found on the git repository](https://gitlab.com/wiechapeter/torchgdm/-/tree/main/examples).
