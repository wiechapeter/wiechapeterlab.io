## Welcome


I am a permanent CNRS researcher ("chargé de recherche") at [LAAS-CNRS](https://www.laas.fr/public/) in Toulouse, France.
My current research is mainly focused on the interaction of light with subwavelength small structures, and on  applications of artificial intelligence in nano-optics and photonics.

After studying physics at the [Technical University of Munich](https://www.tum.de/) in Germany and writing my Diploma thesis at the [Walter Schottky Institute](http://www.wsi.tum.de/), I obtained my PhD in 2016 from the [Université Paul Sabatier Toulouse](https://www.univ-tlse3.fr/) under the supervision of Prof. Vincent Paillard and Dr. Arnaud Arbouet.
2017-2018 I was then postdoc at [CEMES-CNRS](http://www.cemes.fr/) with Dr. Aurélien Cuche and Dr. Christian Girard.
From 2018 until the beginning of 2020, I subsequently held a [DFG research fellowship](http://gepris.dfg.de/gepris/projekt/415025779?language=en) on which I was working with Prof. Otto Muskens in his [integrated nano-photonics group at the University of Southampton](https://inanophotonics.southampton.ac.uk/). 

Please have a look at my [publication list](https://homepages.laas.fr/pwiecha/publications/) or at my [google scholar profile](https://scholar.google.com/citations?user=2iMbkAsAAAAJ&hl=en) to find out more about my research.

<!--[GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.-->
