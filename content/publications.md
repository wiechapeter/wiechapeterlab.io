---
title: Publication list
subtitle: 
comments: false
---


Preprints for most of my publications can be found on [my arxiv page](https://arxiv.org/a/wiecha_p_1.html). I have a [google scholar profile](https://scholar.google.com/citations?user=2iMbkAsAAAAJ&hl=en) and my ORCID is [0000-0002-4571-0116](https://orcid.org/0000-0002-4571-0116).




[<span style="color:blue">Peer-reviewed publications</span>](#peerreview)  
[<span style="color:blue">PhD and master's thesis</span>](#thesis)  
[<span style="color:blue">Other publications</span>](#other)  






## <a name="peerreview"></a>Peer-reviewed publications

### pre-prints

--

### 2024

- Rohit Unni, Mingyuan Zhou, **P. R. Wiecha**, Yuebing Zheng
*Advancing materials science through next-generation machine learning*
[Current Opinion in Solid State & Materials Science, 30, 101157 (Jun. 2024)](https://doi.org/10.1016/j.cossms.2024.101157)

- **P. R. Wiecha**
*Deep learning for nano-photonic materials - The solution to everything!?*
[Current Opinion in Solid State & Materials Science, 28, 101129 (Feb. 2024)](https://doi.org/10.1016/j.cossms.2023.101129) [<span style="color:orange">(arXiv:2310.08618)</span>](https://arxiv.org/abs/2310.08618)

- P. Bennet, D. Langevin, C. Essoual, A. Khaireh-Walieh, O. Teytaud, **P. R. Wiecha**, A. Moreau  
*An illustrated tutorial on global optimization in nanophotonics*  
[JOSA B 41(2), A126-A145 (Feb. 2024)](https://doi.org/10.1364/JOSAB.506389) ([<span style="color:orange">arXiv:2309.09760</span>](https://arxiv.org/abs/2309.09760))  

- R. Hernandez, **P. R. Wiecha**, J.-M. Poumirol, G. Agez, A. Arbouet, L. Ressier, V. Paillard, A. Cuche  
*Directional silicon nano-antennas for quantum emitter control designed by evolutionary optimization*  
[JOSA B 41(2), A108-A115 (Feb. 2024)](https://doi.org/10.1364/JOSAB.506085) ([<span style="color:orange">arXiv:2309.07547</span>](https://arxiv.org/abs/2309.07547))  

- D. Langevin, P. Bennet, A. Khaireh-Walieh, **P. R. Wiecha**, O. Teytaud, A. Moreau  
*PyMoosh : a comprehensive numerical toolkit for computing the optical properties of multilayered structures*  
[JOSA B 41(2), A67-A78 (Feb. 2024)](https://doi.org/10.1364/JOSAB.506175) ([<span style="color:orange">arXiv:2309.00654</span>](https://arxiv.org/abs/2309.00654))  



### 2023

- A. Khaireh-Walieh, D. Langevin, P. Bennet, O. Teytaud, A. Moreau, **P. R. Wiecha**  
*A newcomer's guide to deep learning for inverse design in nano-photonics* [(pdf)](/paper_pdfs/2023_KhairehWalieh_et_al_newcomer.pdf)  
[Nanophotonics 12(24), 4387-4414 (Nov. 2023)](https://doi.org/10.1515/nanoph-2023-0527) [<span style="color:orange">(arXiv:2307.08618)</span>](https://arxiv.org/abs/2307.08618)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Jupyter notebook tutorials](https://gitlab.com/wiechapeter/newcomer_guide_dl_inversedesign)

- W. Xiao, P. Dai, H. J. Singh, I. A. Ajia, **P. R. Wiecha**, R. Huang, C. H. de Groot, O. L. Muskens, K. Sun  
*Flexible thin film optical solar reflectors with Ta2O5-based multimaterial coatings for space radiative cooling* [(pdf)](/paper_pdfs/2023_Xiao_et_al_Flexible.pdf)  
[APL Photonics 8, 090802 (Sept. 2023)](https://doi.org/10.1063/5.0156526)

- A. Estrada-Real, I. Paradisanos, **P. R. Wiecha**, J.-M. Poumirol, A. Cuche, G. Agez, D. Lagarde, X. Marie, V. Larrey, J. Müller, G. Larrieu, V. Paillard, B. Urbaszek  
*Probing the optical near-field interaction of Mie nanoresonators with atomically thin semiconductors*  
[Communications Physics 6(1), 1-7 (May 2023)](https://www.nature.com/articles/s42005-023-01211-2) ([<span style="color:orange">arXiv:2210.14058</span>](https://arxiv.org/abs/2210.14058))  

- A. Khaireh-Walieh, A. Arnoult, S. Plissard, **P. R. Wiecha**  
*Monitoring MBE substrate deoxidation via RHEED image-sequence analysis by deep learning*  
[Crystal Growth and Design, 23(2) 892-898 (Jan. 2023)](https://doi.org/10.1021/acs.cgd.2c01132) ([<span style="color:orange">arXiv:2210.03430</span>](https://arxiv.org/abs/2210.03430))  



### 2022

- A. Estrada-Real, A. Khaireh-Walieh, B. Urbaszek, **P. R. Wiecha**  
*Inverse design with flexible design targets via deep learning: Tailoring of electric and magnetic multipole scattering from nano-spheres*  
[Photonics and Nanostructures - Fundamentals and Applications 52, 101066 (Sept. 2022)](https://www.sciencedirect.com/science/article/pii/S1569441022000761) ([<span style="color:orange">arXiv:2207.03431</span>](https://arxiv.org/abs/2207.03431))  

- Z. Gan, I. Paradisanos, A. Estrada-Real, J. Picker, E. Najafidehaghani, F. Davies, C. Neumann, C. Robert, **P. R. Wiecha**, K. Watanabe, T. Taniguchi, X. Marie, A. V Krasheninnikov, B. Urbaszek, A. George, A. Turchanin  
*Chemical Vapor Deposition of High-Optical-Quality Large-Area Monolayer Janus Transition Metal Dichalcogenides*  
[Advanced Materials 34(38), 2205226 (Aug. 2022)](https://doi.org/10.1002/adma.202205226) ([<span style="color:orange">arXiv:2205.04751</span>](https://arxiv.org/abs/2205.04751))  

- C. Majorel, A. Patoux, A. Estrada-Real, B. Urbaszek, Ch. Girard, A Arbouet, **P. R. Wiecha**  
*Generalizing the exact multipole expansion: Density of multipole modes in complex photonic nanostructures*  
[Nanophotonics 11(16), 3663-3678 (July 2022)](https://doi.org/10.1515/nanoph-2022-0308)  ([<span style="color:orange">arXiv:2204.13402</span>](https://arxiv.org/abs/2204.13402))  

- M. Zhang, J.-M. Poumirol, N. Chery, C. Majorel, R. Demoulin, E. Talbot, H. Rinnert, Ch. Girard, F. Cristiano, **P. R. Wiecha**, T. Hungria, V. Paillard, A. Arbouet, B. Pécassou, F. Gourbilleau, C. Bonafos  
*Infrared nanoplasmonic properties of hyperdoped embedded Si nanocrystals in the few electrons regime*  
[Nanophotonics 11(15), 3485-3493 (July 2022)](https://doi.org/10.1515/nanoph-2022-0283) ([<span style="color:orange">arXiv:2204.13010</span>](https://arxiv.org/abs/2204.13010))  

- K. Sun, E. Vassos, X. Yan, C. Wheeler, J. Churm, **P. R. Wiecha**, S. A. Gregory, A. Feresidis, C. H. de Groot, O. L. Muskens  
*Wafer-Scale 200 mm Metal Oxide Infrared Metasurface with Tailored Differential Emissivity Response in the Atmospheric Windows*  
[Advanced Optical Materials, 2200452 (June 2022)](https://doi.org/10.1002/adom.202200452)

- Y. Brûlé, **P. R. Wiecha**, A, Cuche, V. Paillard, and G. Colas des Francs  
*Magnetic and electric Purcell factor control through geometry optimization of high index dielectric nanostructures*  
[Optics Express 30(12), 20360-20372 (May 2022)](https://doi.org/10.1364/OE.460168) ([<span style="color:orange">arXiv:2201.01734</span>](https://arxiv.org/abs/2201.01734))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Editors' Pick

- C. Majorel, C. Girard, A. Arbouet, O. L. Muskens and **P. R. Wiecha**  
*Deep learning enabled strategies for modelling of complex aperiodic plasmonic metasurfaces of arbitrary size*  
[ACS Photonics, 9(2), 575-585 (Jan. 2022)](https://doi.org/10.1021/acsphotonics.1c01556) ([<span style="color:orange">arXiv:2110.02109</span>](http://arxiv.org/abs/2110.02109))  

- M. Humbert, **P. R. Wiecha**, G. Colas des Francs, X. Yu, N. Mallet, A. Lecestre, G. Larrieu, V. Larrey, F. Fournel, L. Ressier, C. Girard, V. Paillard and A. Cuche  
*Tailoring Wavelength- and Emitter-Orientation-Dependent Propagation of Single Photons in Silicon Nanowires*  
[Phys. Rev. Applied 17, 014008 (Jan. 2022)](https://doi.org/10.1103/PhysRevApplied.17.014008)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Editors' Suggestion

- **P. R. Wiecha**, C. Majorel, A. Arbouet, A. Patoux, Y. Brûlé, G. Colas des Francs, C. Girard  
*pyGDM - new functionalities and major improvements to the python toolkit for nano-optics full-field simulations*  
[Computer Physics Communications 108142 (Jan. 2022)](https://doi.org/10.1016/j.cpc.2021.108142) ([<span style="color:orange">arXiv:2105.04587</span>](http://arxiv.org/abs/2105.04587))  



### 2021

- J.-M. Poumirol, C. Majorel, N. Chery, C. Girard, **P. R. Wiecha**, N. Mallet, G. Larrieu, F. Cristiano, R. Monflier, A.-S. Royet, P. Acosta Alba, S. Kerdiles, V. Paillard and C. Bonafos  
*Hyper-doped silicon nanoantennas and metasurfaces for tunable infrared plasmonics*  
[ACS Photonics 8(5), (May 2021)](http://doi.org/10.1021/acsphotonics.1c00019) ([<span style="color:orange">arXiv:2011.07779</span>](http://arxiv.org/abs/2011.07779))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Highlighted by CEMES-CNRS](https://www.cemes.fr/Nano-silicon-antennas-to-capture?lang=en)

- **P. R. Wiecha**, A. Arbouet, C. Girard and O. L. Muskens  
*Deep learning in nano-photonics: inverse design and beyond*  
[Photonics Research 9(3), (Mar. 2021)](http://doi.org/10.1364/PRJ.415960) ([<span style="color:orange">arXiv:2011.12603</span>](http://arxiv.org/abs/2011.12603))  

- A. Patoux, G. Agez, Ch. Girard, V. Paillard, **P. R. Wiecha**, A. Lecestre, F. Carcenac, G. Larrieu, A. Arbouet  
*Challenges in Nanofabrication for Efficient Optical Metasurfaces*  
[Scientific Reports 11(1), 5620 (Mar. 2021)](https://doi.org/10.1038/s41598-021-84666-z)  

- N. J. Dinsdale, **P. R. Wiecha**, M. Delaney, J. Reynolds, M. Ebert, I. Zeimpekis, D. J. Thomson, G. T. Reed, P. Lalanne, K. Vynck and O. L. Muskens  
*Deep learning enabled design of complex transmission matrices for universal optical components*  
[ACS Photonics 8(1), 283-295 (Jan. 2021)](http://dx.doi.org/10.1021/acsphotonics.0c01481) ([<span style="color:orange">arXiv:2009.11810</span>](http://arxiv.org/abs/2009.11810))  



### 2020

- J.-M. Poumirol, I. Paradisanos, S. Shree, G. Agez, X. Marie, C. Robert, N. Mallet, **P. R. Wiecha**, G. Larrieu, V. Larrey, F. Fournel, K. Watanabe, T. Taniguchi, A. Cuche, V. Paillard and B. Urbaszek  
*Unveiling the optical emission channels of monolayer semiconductors coupled to silicon nanoantennas*  
[ACS Photonics, 7(11), 3106-3115 (Sept. 2020)](https://doi.org/10.1021/acsphotonics.0c01175) ([<span style="color:orange">arXiv:2007.12612</span>](http://arxiv.org/abs/2007.12612))  

- A. Patoux, C. Majorel, **P. R. Wiecha**, A. Cuche, O. L. Muskens, C. Girard and A. Arbouet  
*Polarizabilities of complex individual dielectric or plasmonic nanostructures*  
[Physical Review B 101(23), 235418 (June 2020)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.101.235418) ([<span style="color:orange">arXiv:1912.04124</span>](http://arxiv.org/abs/1912.04124))  

- C. Majorel, C. Girard, A. Cuche, A. Arbouet and **P. R. Wiecha**  
*Quantum Theory of Near-field Optical Imaging with Rare-earth Atomic Clusters*  
[JOSA B 37(5), 1474-1484 (Apr. 2020)](https://doi.org/10.1364/JOSAB.385918) ([<span style="color:orange">arXiv:1912.06023</span>](http://arxiv.org/abs/1912.06023))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted as Editors' Pick

- **P. R. Wiecha** and O. L. Muskens.  
*Deep learning meets nanophotonics: A generalized accurate predictor for near fields and far fields of arbitrary 3D nanostructures*  
[Nano Letters 20(1), p. 329-338 (Jan. 2020)](https://doi.org/10.1021/acs.nanolett.9b03971) ([<span style="color:orange">arXiv:1909.11822</span>](http://arxiv.org/abs/1909.12056))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by the [University of Southampton](https://www.phys.soton.ac.uk/news/6599)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by the [IOP journal "*Physics World*"](https://physicsworld.com/a/artificial-intelligence-reveals-how-light-flows-around-nanoparticles/)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by [AZoNano](https://www.azonano.com/news.aspx?newsID=37116)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Dataset](https://doi.org/10.5258/SOTON/D1088)



### 2019

- **P. R. Wiecha**, C. Majorel, Ch. Girard, A. Cuche, V. Paillard, O. L. Muskens and A. Arbouet.  
*Design of Plasmonic Directional Antennas via Evolutionary Optimization*  
[Optics Express 27(20) (Sept. 2019) p. 29069-29081](https://doi.org/10.1364/OE.27.029069) ([<span style="color:orange">arXiv:1906.11822</span>](http://arxiv.org/abs/1906.11822))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Dataset](https://eprints.soton.ac.uk/433390/)

- C. Majorel, V. Paillard, A. Patoux, **P. R. Wiecha**, A. Cuche, A. Arbouet, C. Bonafos and C. Girard  
*Theory of plasmonic properties of hyper-doped silicon nanostructures*  
[Optics Communications 124336 (Aug. 2019)](https://doi.org/10.1016/j.optcom.2019.124336)  

- U. Kürüm, **P. R. Wiecha**, R. French and O. L. Muskens.  
*Deep Learning Enabled Real Time Speckle Recognition and Hyperspectral Imaging using a Multimode Fiber Array*  
[Optics Express 27(15) (July 2019) p. 20965-20979](https://doi.org/10.1364/OE.27.020965) ([<span style="color:orange">arXiv:1904.04673</span>](http://arxiv.org/abs/1904.04673))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Dataset](https://eprints.soton.ac.uk/433392/)

- **P. R. Wiecha**, A. Lecestre, N. Mallet, and G. Larrieu.  
*Pushing the limits of optical information storage using deep learning*  
[Nature Nanotechnology 14(3) (Mar. 2019), 237-244](https://www.nature.com/articles/s41565-018-0346-1) ([<span style="color:orange">arXiv:1805.03468</span>](https://arxiv.org/abs/1805.03468))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted in ["*News and Views*"](https://www.nature.com/articles/s41565-018-0357-y)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by the [IOP journal "*Physics World*"](https://physicsworld.com/a/deep-learning-improves-optical-storage/)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by the [CNRS](https://www.cnrs.fr/occitanie-ouest/actualites/article/actualite-scientifique-repousser-les-limites-du-stockage-de-donnees-grace-a-l) and [French Institute of Physics](https://inp.cnrs.fr/fr/cnrsinfo/repousser-les-limites-du-stockage-de-donnees-grace-lapprentissage-automatique)

- **P. R. Wiecha**, C. Majorel, C. Girard, A. Arbouet, B. Masenelli, O. Boisron, A. Lecestre, G. Larrieu, V. Paillard, and A. Cuche.  
*Enhancement of electric and magnetic dipole transition of rare-earth doped thin films tailored by high-index dielectric nanostructures*  
[Appl. Opt. 58(7) (Feb. 2019) p. 1682-1690](https://doi.org/10.1364/AO.58.001682) ([<span style="color:orange">arXiv:1801.09690</span>](http://arxiv.org/abs/1801.09690))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted as Editors' Pick
 


### 2018

- **P. R. Wiecha**  
*pyGDM - A python toolkit for full-field electro-dynamical simulations and evolutionary optimization of nanostructures*  
[Computer Physics Communications 233 (Dec. 2018). p 167-192](https://doi.org/10.1016/j.cpc.2018.06.017) ([<span style="color:orange">arXiv:1802.04071</span>](https://arxiv.org/abs/1802.04071))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [gitlab repository](https://gitlab.com/wiechapeter/pyGDM2)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [online documentation](https://wiechapeter.gitlab.io/pyGDM2-doc/)

- C. Girard, **P. R. Wiecha**, A. Cuche, and E. Dujardin.  
*Designing Thermoplasmonic Properties of Metallic Metasurfaces*  
[Journal of Optics 20(7) (May 2018) p. 075004](https://doi.org/10.1088/2040-8986/aac934) ([<span style="color:orange">arXiv:1804.01111</span>](https://arxiv.org/abs/1804.01111))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by [CNRS Institute of Physics](https://inp.cnrs.fr/fr/cnrsinfo/un-algorithme-pour-faconner-des-proprietes-thermiques-lechelle-nanometrique)

- **P. R. Wiecha**, C. Girard, A. Cuche, V. Paillard, and A. Arbouet.  
*Decay Rate of Magnetic Dipoles near Non-magnetic Nanostructures*  
[Physical Review B 97 (Feb. 2018). p. 085411](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.97.085411) ([<span style="color:orange">arXiv:1707.07006</span>](http://arxiv.org/abs/1707.07006))

 
 
### 2017
 
- **P. R. Wiecha**, M.-M. Mennemanteuil, D. Khlopin, J. Martin, A. Arbouet, D. Gérard, A. Bouhelier, J. Plain, and A. Cuche  
*Local field enhancement and thermoplasmonics in multimodal Aluminum structures*  
[Physical Review B 96 (July 2017). p. 035440](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.96.035440) ([<span style="color:orange">arXiv:1707.02983</span>](https://arxiv.org/abs/1707.02983))
 
- **P. R. Wiecha**, A. Cuche, A. Arbouet, C. Girard, G. Colas des Francs, A. Lecestre, G. Larrieu, F. Fournel, V. Larrey, T. Baron, and V. Paillard  
*Strongly directional scattering from dielectric nanowires*  
[ACS Photonics 4(8) (July 2017). p. 2036-2046](http://dx.doi.org/10.1021/acsphotonics.7b00423) ([<span style="color:orange">arXiv:1704.07361</span>](https://arxiv.org/abs/1704.07361))

- **P. R. Wiecha**, A. Arbouet, C. Girard, A. Lecestre, G. Larrieu, and V. Paillard  
*Evolutionary multi-objective optimization of colour pixels based on dielectric nanoantennas*  
[Nature Nanotechnology 12(2) (Feb. 2017). p. 163-169](http://dx.doi.org/10.1038/nnano.2016.224) ([<span style="color:orange">arXiv:1609.06709</span>](https://arxiv.org/abs/1609.06709))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Highlighted by [CNRS INSIS](http://www.cnrs.fr/insis/recherche/actualites/2016/11/nanostructures-photoniques.htm)

- **P. R. Wiecha**, L.-J. Black, Y. Wang, V. Paillard, C. Girard, O. L. Muskens, and A. Arbouet  
*Polarization conversion in plasmonic nanoantennas for metasurfaces using structural asymmetry and mode hybridization*  
[Scientific Reports 7 (Jan. 2017). p. 40906](http://dx.doi.org/10.1038/srep40906)



### 2016

- **P. R. Wiecha**, A. Arbouet, C. Girard, T. Baron, and V. Paillard  
*Origin of second-harmonic generation from individual silicon nanowires*  
[Physical Review B 93(12) (Mar. 2016). p. 125421](http://dx.doi.org/10.1103/PhysRevB.93.125421) ([<span style="color:orange">arXiv:1510.05409</span>](https://arxiv.org/abs/1510.05409))



### 2015

- L.-J. Black, **P. R. Wiecha**, Y. Wang, C. H. de Groot, V. Paillard, C. Girard, O. L. Muskens, and A. Arbouet  
*Tailoring Second-Harmonic Generation in Single L-Shaped Plasmonic Nanoantennas from the Capacitive to Conductive Coupling Regime*  
[ACS Photonics 2(11) (Nov. 2015). p. 1592-1601](http://dx.doi.org/10.1021/acsphotonics.5b00358)

- **P. R. Wiecha**, A. Arbouet, H. Kallel, P. Periwal, T. Baron, and V. Paillard  
*Enhanced nonlinear optical response from individual silicon nanowires*  
[Physical Review B 91(12) (Mar. 2015). p. 121416](http://dx.doi.org/10.1103/PhysRevB.91.121416) ([<span style="color:orange">arXiv:1704.03818</span>](https://arxiv.org/abs/1704.03818))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Rapid Communication  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Editor's suggestion



### 2013

- J. Treu, M. Bormann, H. Schmeiduch, M. Döblinger, S. Morkötter, S. Matich, **P. Wiecha**, K. Saller, B. Mayer, M. Bichler, M.-C. Amann, J. J. Finley, G. Abstreiter, and G. Koblmüller.  
*Enhanced Luminescence Properties of InAs–InAsP Core–Shell Nanowires*  
[Nano Letters 13(12) (Dec. 2013). p. 6070-6077](http://dx.doi.org/10.1021/nl403341x)

- S. Sprengel, C. Grasse, **P. Wiecha**, A. Andrejew, T. Gruendl, G. Boehm, R. Meyer, and M. Amann  
*InP-Based Type-II Quantum-Well Lasers and LEDs*  
[IEEE Journal of Selected Topics in Quantum Electronics 19(4) (July 2013). p. 1900909-1900909](https://doi.org/10.1109/JSTQE.2013.2247572)

- C. Grasse, T. Gruendl, S. Sprengel, **P. Wiecha**, K. Vizbaras, R. Meyer, and M.-C. Amann  
*GaInAs/GaAsSb-based type-II micro-cavity LED with 2–3 μm light emission grown on InP substrate*  
[Journal of Crystal Growth 370 (May 2013). p. 240-243](http://dx.doi.org/10.1016/j.jcrysgro.2012.07.001)

- C. Grasse, M. Mueller, T. Gruendl, G. Boehm, E. Roenneberg, **P. Wiecha**, J. Rosskopf, M. Ortsiefer, R. Meyer, and M.-C. Amann  
*AlGaInAsPSb-based high-speed short-cavity VCSEL with single-mode emission at 1.3 μm grown by MOVPE on InP substrate*  
[Journal of Crystal Growth 370 (May 2013). p. 217-220](http://dx.doi.org/10.1016/j.jcrysgro.2012.06.051)



### 2012

- C. Grasse, **P. Wiecha**, T. Gruendl, S. Sprengel, R. Meyer, and M.-C. Amann  
*InP-based 2.8–3.5 μm resonant-cavity light emitting diodes based on type-II transitions in GaInAs/GaAsSb heterostructures*  
[Applied Physics Letters 101(22) (Nov. 2012). p. 221107](http://dx.doi.org/10.1063/1.4768447)



## <a name="thesis"></a>PhD Thesis, Master's Thesis

- **P. R. Wiecha**  
*Linear and nonlinear optical properties of high refractive index dielectric nanostructures*  
[PhD Thesis at Université Toulouse III](http://thesesups.ups-tlse.fr/3361/) (2016)

- **P. R. Wiecha**  
*Mid-Infrared Light Generation on InP*  
[Diploma Thesis at Walter Schottky Institute, TU Munich](https://www.researchgate.net/profile/Peter_Wiecha/publication/315982686_Masters_thesis_Mid-Infrared_Light_Generation_on_InP/links/58ed3460aca2724f0a26c451/Masters-thesis-Mid-Infrared-Light-Generation-on-InP.pdf) (2012)



## <a name="other"></a>Book chapters

- **P. R. Wiecha**, N. J. Dinsdale, and O. L. Muskens
*Deep Learning Driven Data Processing, Modeling, and Inverse Design for Nanophotonics*  
Chapter in: [Integrated Nanophotonics,  Pp. 245–275, Ed. by Peng Yu, John Wiley & Sons (Jun. 2023)](https://doi.org/10.1002/9783527833030.ch7) ([<span style="color:orange">hal-04628291</span>](https://hal.science/hal-04628291))

- **P. R. Wiecha** , A. Cuche, H. Kallel, G. Colas des Francs, A. Lecestre, G. Larrieu, V. Larrey, F. Fournel, T. Baron, A. Arbouet, and V. Paillard  
*Fano-Resonances in High Index Dielectric Nanowires for Directional Scattering*  
Chapter in: [Fano resonances in optics and microwaves: Physics and application. Ed. by E. O. Kamenetskii, A. Sadreev, and A. Miroshnichenko. Springer. (Jan. 2019).](https://www.springer.com/us/book/9783319997308) ([<span style="color:orange">arXiv:1801.05601</span>](https://arxiv.org/abs/1801.05601))

- O. L. Muskens, **P. R. Wiecha**, and A. Arbouet  
*Ultrafast Spectroscopy and Nonlinear Control of Single Nanoparticles and Antennas*  
Chapter in: [Handbook of Metamaterials and Nanophotonics – Nanoplasmonics. Ed. by J. Aizpurua (general Ed.: S. A. Maier). World Scientific. (Dec. 2017)](https://doi.org/10.1142/9789813228726_0006)



## <a name="other"></a>Proceedings / other publications

- J. Jiminez-Jaimes, S. Ponomareva, **P. R. Wiecha**
*Deep learning for inverse design of dielectric nanostructures with distinguishable RGB color signatures on dark-field microscopy*
[Proceedings of SPIE Machine Learning in Photonics, 13017, p. 40-46 (May 2024)](https://doi.org/10.1117/12.3014728)

- **P. R. Wiecha**, A. Arbouet, C. Girard, A. Lecestre, G. Larrieu, and V. Paillard
*Multi-Resonant Silicon Nanoantennas by Evolutionary Multi-Objective Optimization*
[Proceedings of SPIE Computational Optics, 10694, p. 1069402 (May 2018)](http://dx.doi.org/10.1117/12.2315123)

- H. Kallel, **P. Wiecha**, Y. Zhao, A. Arbouet, M. Carrada, G. B. Assayag, P. Periwal, T. Baron, P. Normand, A. Chehaidar, and V. Paillard  
*Photoluminescence Enhancement of a Silicon Nanocrystal Plane Positioned in the Near-Field of a Silicon Nanowire (invited)*  
[ECS Transactions 61.5 (Mar. 2014). Pp. 189-197](http://dx.doi.org/10.1149/06105.0189ecst)

