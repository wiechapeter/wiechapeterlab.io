---
title: Contact
subtitle: Peter R. Wiecha
comments: false
---

If you would like to contact me, please write me an email: pwiecha *-at-* laas *-dot-* fr.

You can also find me on [arxiv](https://arxiv.org/a/wiecha_p_1.html), [researchgate](https://www.researchgate.net/profile/Peter_Wiecha), [google scholar](https://scholar.google.com/citations?user=2iMbkAsAAAAJ&hl=en) and [ORCID](https://orcid.org/0000-0002-4571-0116).

My postal address is:

Office D7  
LAAS-CNRS  
7 Avenue du Colonel Roche  
31400 Toulouse  
France
