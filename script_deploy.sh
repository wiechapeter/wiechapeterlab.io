#!/bin/bash


echo "removing former build..."
rm -r public

echo "building website..."
hugo

echo "copy to http server via sftp..."


rsync -r public/* laas_homepages:/home/pwiecha/
echo "Done. Please check. It may be necessary to clean server first and redo."
